//#include <cstdlib>
//#include <iostream>
//#include <valarray>
//using namespace std;
//#include "DRS.h"


#if defined(ARDUINO) && ARDUINO >= 100
	#include "Arduino.h"
#else
	#include "WProgram.h"
#endif

#include "DSR.h"
/* ----------------------------------------------------------  */
/* --------------- LONG ADDRESS FUNCTIONS--------------------  */
/* ----------------------------------------------------------  */

LongAddress::LongAddress(){
    this->P1address = 0;
    this->P2address = 0;
}
LongAddress::LongAddress(uint32_t P1, uint32_t P2){
    this->P1address = P1;
    this->P2address = P2;
}
/*void LongAddress::print(){
    cout << " long : " << hex << this->P1address << ","  << hex << this->P2address;
}*/

/* ----------------------------------------------------------  */
/* ---------------SMALL ADDRESS -----------------------------  */
/* ----------------------------------------------------------  */

SmallAddress::SmallAddress(){}
SmallAddress::SmallAddress(char smallAddress[SMALL_ADDRESS_SIZE]){
	for(int i=0; i< SMALL_ADDRESS_SIZE; i++)
			this->address[i] = smallAddress[i];
}

void SmallAddress::setAddress(char smallAddress[SMALL_ADDRESS_SIZE]){

    for(int i=0; i< SMALL_ADDRESS_SIZE; i++)
        this->address[i] = smallAddress[i];
}
/*void SmallAddress::print(){

    for(int i=0; i< SMALL_ADDRESS_SIZE; i++)
        cout << this->address[i];
}*/
bool operator==(SmallAddress const& a, SmallAddress const& b){

    if((a.address[0] == b.address[0]) && (a.address[1] == b.address[1]))
        return true;
    else
        return false;
}

/* ----------------------------------------------------------  */
/* ----------------- MESSAGE FUNCTIONS ----------------------  */
/* ----------------------------------------------------------  */

/*void Message::print(){
    cout << " ---- Message ----" << endl;
    cout << "type : " << this->type << endl;
    cout << "source : "; this->source.print(); cout << endl;
    cout << "numero message : " << this->numMessage << endl;
    cout << "destinataire : "; this->destination.print(); cout << endl;
    cout << "liste  ";
    for(int i = 0; i<this->routeSize; i++) {
        cout << " ; "; this->route[i].print();;
    }
    if(this->type == TYPE_DATA) {
        cout << "taille message : " << this->messageSize;
        cout << endl << "message : ";
        for(int i = 0; i< this->messageSize; i++)
            cout << this->message[i];
        cout << endl;
        //cout << "taille message : " << this->messageSize;
    }

    cout << endl << "------------------" << endl;
}*/

/* ----------------------------------------------------------  */
/* --------------- READY MESSAGE  ---------------------------  */
/* ----------------------------------------------------------  */

ReadyMessage::ReadyMessage(){
    for(int i = 0; i< MESSAGE_TOTAL_MAX_SIZE; i++)
        this->message[i] = '\0';
    this->size = 0;
}
/*void ReadyMessage::print(){
    cout << "Message to send :" << endl;
    cout << "taille = " << this->size << endl;
    cout << "chaine = ";
    for(int i=0; i<this->size; i++)
        cout << this->message[i];
    cout << endl;
    cout << "destinataire : "; this->dest.print();
    cout << endl;
    cout << "------------------" << endl;
}*/

/* ----------------------------------------------------------  */
/* -------------- SAVED MESSAGE -----------------------------  */
/* ----------------------------------------------------------  */

SavedMessage::SavedMessage(){
}
SavedMessage::SavedMessage(SmallAddress sender, SmallAddress receiver, char numMessage, char typeMessage){
    this->sender.address[0] = sender.address[0];
    this->sender.address[1] = sender.address[1];
    this->receiver.address[0] = receiver.address[0];
    this->receiver.address[1] = receiver.address[1];
    this->numMessage = numMessage;
    this->messageType = typeMessage;
}
/*void SavedMessage::print(){
    cout << endl << "Message : " << endl;
    cout << " - source : "; this->sender.print(); cout << endl;
    cout << " - destination : ";  this->receiver.print(); cout << endl;
    cout << " - numero : " << this->numMessage << endl;
    cout << " - type : " << this->messageType << endl;
    cout << "------------------" << endl;
}*/
bool operator==(SavedMessage const& a, SavedMessage const& b){

    if ((b.numMessage == a.numMessage) && (b.receiver == a.receiver) && (b.sender == a.sender) && (a.messageType == b.messageType))
        return true;
    else
        return false;
}

/* ----------------------------------------------------------  */
/* ------------------- Network Simulation -------------------  */
/* ----------------------------------------------------------  */

NetworkSimulation::NetworkSimulation(){
    this->_etat = false;
    this->_whiteListSize = 0;
}
void NetworkSimulation::startNetworkSimulation(){
    this->_etat = true;
}
void NetworkSimulation::stopNetworkSimulation(){
    this->_etat = false;
}
bool NetworkSimulation::getEtat(){
    return this->_etat;
}
void NetworkSimulation::addToWhiteList(SmallAddress address){
    this->_whiteList[this->_whiteListSize] = address;
    this->_whiteListSize++;
}
void NetworkSimulation::supprOfWhiteList(SmallAddress address){
    bool stop = false;
    for(int i=0 ; i<this->_whiteListSize && stop == false; i++){ //for each white list element
        if(this->_whiteList[i]==address) //test
        {
            stop = true;
            this->_whiteListSize--;

            if(i < this->_whiteListSize){ //not end of list
                this->_whiteList[i] = this->_whiteList[this->_whiteListSize];
            }

        }
    }
}
bool NetworkSimulation::canPass(SmallAddress address){
    for(int i =0; i< this->_whiteListSize; i++){ //for each white list element
        if(this->_whiteList[i]==address) //test
            return true;
    }
    return false;
}
/*void NetworkSimulation::print(){
    cout << "white liste size : " << this->_whiteListSize << endl;
    cout << "started ? : " << this->_etat << endl;

    for(int i = 0; i<this->_whiteListSize; i++){
        this->_whiteList[i].print(); cout << ";";
    }
    cout << endl << "--------------------" << endl;
}*/


/* ----------------------------------------------------------  */
/* ---------------------- Address Manager -------------------  */
/* ----------------------------------------------------------  */
AddressManager::AddressManager(){
    this->_sizeOfSavedAddress = 0;
}

bool AddressManager::_isADuplicatedAddress(AddressMemElement address){
    // if is duplicated return true
    for(int i = 0; i<this->_sizeOfSavedAddress; i++) {

        int verif = 0;

        for(int j = 0; j < SMALL_ADDRESS_SIZE; j++) {
            if(this->_listOfAddress[i].identifiant.address[j] == address.identifiant.address[j])
                verif++;
        }

        if(verif == SMALL_ADDRESS_SIZE)
            return true;
    }
    return false;
}

LongAddress AddressManager::getConrrespondingLongAddress(SmallAddress smallAddress){
    //search the corresponding long address
    for(int i = 0; i<this->_sizeOfSavedAddress; i++) {
        if(this->_listOfAddress[i].identifiant == smallAddress)
            return this->_listOfAddress[i].longAddress;
    }

    LongAddress longAddressDef;
    return longAddressDef;
}
void AddressManager::addAddress(AddressMemElement address){
    if(!this->_isADuplicatedAddress(address) && this->_sizeOfSavedAddress < MAX_ADDRESS_MEMORY_SIZE) {
        this->_listOfAddress[this->_sizeOfSavedAddress] = address;
        this->_sizeOfSavedAddress++;
    }
}
/*void AddressManager::print(){
    cout << "Taille du tableau adresse : " << this->_sizeOfSavedAddress << endl;
    for (int i=0; i<this->_sizeOfSavedAddress; i++) {
        this->_listOfAddress[i].identifiant.print();
        this->_listOfAddress[i].longAddress.print();
    }
}*/

/* ----------------------------------------------------------  */
/* ------------------ Old Message Manager -------------------  */
/* ----------------------------------------------------------  */

OldMessageManager::OldMessageManager(){
    this->_sizeListOldMessage = 0;
    this->_iteratorListOldMessage = 0;
}

SavedMessage& OldMessageManager::getOldSavedMessage(){
    return *this->_listOldMessage;
}
int OldMessageManager::getSizeList(){
    return this->_sizeListOldMessage;
}
int OldMessageManager::getIteratorListOldMessage(){
    return this->_iteratorListOldMessage;
}

bool OldMessageManager::addNewMessage(SavedMessage message){

    if (this->_sizeListOldMessage == 0) {
        this->_listOldMessage[0] = message;
        this->_sizeListOldMessage++;
    }else{
        for(int i=0; i<this->_sizeListOldMessage; i++) {
            if(message == this->_listOldMessage[i])
                return true;
        }
        if(this->_sizeListOldMessage == OLD_MESSAGES_BUFFER_SIZE) {
            this->_listOldMessage[this->_iteratorListOldMessage] = message;
            this->_iteratorListOldMessage++;
            if(this->_iteratorListOldMessage == OLD_MESSAGES_BUFFER_SIZE)
                this->_iteratorListOldMessage = 0;
        }
        else{
            this->_listOldMessage[_sizeListOldMessage] = message;
            this->_sizeListOldMessage++;
        }
    }
    return false;
}
/*void OldMessageManager::print(){
    cout << "Taille du tableau de vieux Message: " << this->_sizeListOldMessage << endl;
    for (int i=0; i<this->_sizeListOldMessage; i++)
        this->_listOldMessage[i].print();
    cout << "------------------" << endl;
}*/


/* ----------------------------------------------------------  */
/* ------------------- DSR FUNCTIONS ------------------------  */
/* ----------------------------------------------------------  */

//CONSTRUCTOR
DSRprotocol::DSRprotocol(char selfAddress[SMALL_ADDRESS_SIZE]){
    // variables initiation
    this->_role = 0;
    this->_whatToDo = 0;
    this->_selfAddress.setAddress(selfAddress);
    this->_message.numMessage = 0;
    this->_numberSendedMessage = '0';
    this->_sizeDta = 0;
    this->sendingStarted = false;
    for(int i; i<MESSAGE_CONTENT_MAX_SIZE; i++){
        this->_data[i] = '\0';
    }
}

int DSRprotocol::_wayTreatment(uint8_t message[]){

    if(this->_message.type == TYPE_PREQ || this->_message.type == TYPE_PREP) {

        int j=0;
        bool end =false;

        // get information about the way
        for(int i=ROUTE_START; (i < MESSAGE_START)&&((message[i] != DATA_NULL) && (message[i+1] != DATA_NULL))&&(i < this->_message.size) && !end; i=i+SMALL_ADDRESS_SIZE) {

            char elemRoute[SMALL_ADDRESS_SIZE];

            for(int k=0; k<SMALL_ADDRESS_SIZE; k++) {

                //not null address verification
                if(message[i+k] != DATA_NULL && !end)
                    elemRoute[k] = message[i+k];
                else
                    end = true;
            }

            this->_message.route[j].setAddress(elemRoute);
            j++;
        }

        this->_message.routeSize = j;

        // if path is too long, it's not possible to do something with it.
        if(j >= PATH_MAX_SIZE)
            return NOTHING;

    }else if(this->_message.type == TYPE_DATA) {

        int j=0;

        for(int i=ROUTE_START; ((i < MESSAGE_START)&&((message[i] != DATA_NULL) && (message[i+1] != DATA_NULL))); i=i+SMALL_ADDRESS_SIZE) {

            char elemRoute[SMALL_ADDRESS_SIZE];

            for(int k=0; k<SMALL_ADDRESS_SIZE; k++)
                elemRoute[k] = message[i+k];

            this->_message.route[j].setAddress(elemRoute);
            j++;
        }

        this->_message.routeSize = j;
    }

    return 1;
}

int DSRprotocol::_set_Role(){

    //define the role of the recipient
    //return (0 = NOTHING / 1 = RECIPIENT  / 2 = MEDIATOR)

    SavedMessage newSavedMessage(this->_message.source, this->_message.destination, this->_message.numMessage, this->_message.type);
    bool duplicated = this->_oldMessageManager.addNewMessage(newSavedMessage);
    if(duplicated) {
        this->_role = 0;
        return 0;
    }
    if(this->_selfAddress == this->_message.destination) {
        this->_role = 1;
        return 1;
    }
    if(this->_message.type == TYPE_PREP && this->_selfAddress == this->_message.source){
        this->_role = 1;

        return 1;
    }
    if(this->_message.type == TYPE_DATA && this->_selfAddress == this->_message.source){
        this->_role = 1;
        return 1;
    }

    this->_role = 2;
    return 2;

}
void DSRprotocol::_addYourselfToWayList(){
    for(int i =0; i<SMALL_ADDRESS_SIZE; i++)
        this->_message.route[_message.routeSize].address[i] = this->_selfAddress.address[i];

    this->_message.routeSize++;
}
void DSRprotocol::_startRebuild(ReadyMessage *response){
    //convert Message to a char array
    int size = 0;

    if(this->_whatToDo != NOTHING){
        response->message[TYPE_START]=this->_message.type;
        response->message[NUM_START]=this->_message.numMessage;
        size = 2;

        for(int i = 0; i<SMALL_ADDRESS_SIZE; i++) {
            response->message[i+SOURCE_START] = this->_message.source.address[i];
            response->message[i+DEST_START] = this->_message.destination.address[i];
            size = size+2;
        }
        for(int i = 0; i< this->_message.routeSize; i++) {
            for(int j = 0; j<SMALL_ADDRESS_SIZE; j++) {
                response->message[j+(i*SMALL_ADDRESS_SIZE)+ROUTE_START] = this->_message.route[i].address[j];
                size++;
            }
        }

        if(this->_message.type == TYPE_DATA) {
            for(int i = 0; i<this->_message.messageSize; i++)
                response->message[i + MESSAGE_START] = this->_message.message[i];
            response->size = MESSAGE_START + this->_message.messageSize;

        }else
            response->size=size;
    }
}

LongAddress DSRprotocol::_getGoodLongAddress(int need) {
    //if need the address of last element of the way list
    if(need == LAST){
        if(this->_message.routeSize == 0)
            return this->_addressManager.getConrrespondingLongAddress(this->_message.source);
        else
            return this->_addressManager.getConrrespondingLongAddress(this->_message.route[this->_message.routeSize-1]);
    }
    //if need the first one
    if(need == FIRST){
        return this->_addressManager.getConrrespondingLongAddress(this->_message.route[0]);
    }
    if(need == BEFOREME || need == AFTERME) {

        bool stop = false;
        int i = 0;
        // find herself and his position in the way list
        while(i<this->_message.routeSize && !stop) {
            if(this->_message.route[i] == this->_selfAddress)
                stop = true;
            else
                i++;
        }

        //get address before it
        if(need == BEFOREME){
            if(i == 0)
                return this->_addressManager.getConrrespondingLongAddress(this->_message.source);
            return this->_addressManager.getConrrespondingLongAddress(this->_message.route[i-1]);
        }
        else{ //get address after it
            return this->_addressManager.getConrrespondingLongAddress(this->_message.route[i+1]);
        }
    }

    LongAddress longAddress(BROADCAST1,BROADCAST2);
    return longAddress;
}

// Read the table to make an instance message (better for manipulation) and return what you need to do
int DSRprotocol::firstMessageProcessing(uint8_t message[], int size, AddressMemElement addressMemElement){

    this->_message.size = 0;
    this->_message.routeSize = 0;
    this->_whatToDo = NOTHING;

    //NetworkSimulation
    if(this->networkSimulator.getEtat())
    {
        if(!this->networkSimulator.canPass(addressMemElement.identifiant))
           return NOTHING;
    }

    // Test if it's a true message
    if((message[0] != TYPE_PREQ) && (message[0]!= TYPE_PREP) && (message[0] != TYPE_DATA))
        return NOTHING;

    char source[SMALL_ADDRESS_SIZE];
    char dest[SMALL_ADDRESS_SIZE];

    // Get source and dest information
    for(int i=0; i<SMALL_ADDRESS_SIZE; i++) {
        source[i] = message[SOURCE_START+i];
        dest[i] = message[DEST_START+i];
    }

    this->_message.type = message[TYPE_START];
    this->_message.size = size;
    if(this->_wayTreatment(message) == NOTHING)
        return NOTHING;

    //Get other message information
    this->_message.source.setAddress(source);
    this->_message.numMessage = message[NUM_START];
    this->_message.destination.setAddress(dest);

    //Get data
    if(this->_message.type == TYPE_DATA) {
        int i = 0;
        for(i; i<size && message[i + MESSAGE_START] != DATA_NULL; i++)
            this->_message.message[i] = message[i + MESSAGE_START];
        this->_message.messageSize = i;
    }

    //check the user role
    //if role = 0 it's not necessary to check the way and to do something
    if(this->_set_Role() == 0)
        return NOTHING;

    this->_addressManager.addAddress(addressMemElement);

    //important to know the exact message type.
    this->_whatToDo = getWhatToDo();
    return this->_whatToDo;

}
int DSRprotocol::getWhatToDo(){
    //message is a type_rep messate
    if(this->_role == 1 && this->_message.type == TYPE_PREQ)
        return PREQ_DEST;
    //message is a PREQ_INTER message
    if(this->_role == 2 && this->_message.type == TYPE_PREQ)
        return PREQ_INTER;
    //message is a PREP_DEST message
    if(this->_role == 1 && this->_message.type == TYPE_PREP)
        return PREP_DEST;
    //message is a PREP_INTER message
    if(this->_role == 2 && this->_message.type == TYPE_PREP)
        return PREP_INTER;
    //message is a DATA_DEST message
    if(this->_role == 1 && this->_message.type == TYPE_DATA){
        this->sendingStarted = false;
        return DATA_DEST;
    }
    //message is a DATA_INTER message
    if(this->_role == 2 && this->_message.type == TYPE_DATA)
        return DATA_INTER;
    //message is a DATA_SEND message
    if(this->_role == 3 && this->_message.type == TYPE_DATA)
        return DATA_SEND;

    return NOTHING;
}

ReadyMessage DSRprotocol::getResponseToSend(){

    ReadyMessage message;

    if(this->_whatToDo == PREQ_DEST) {
        message.dest=this->_getGoodLongAddress(LAST);
        this->_addYourselfToWayList();
        this->_message.type = TYPE_PREP; //need to change type
        this->_message.numMessage++;
        SavedMessage newSavedMessage(this->_message.source, this->_message.destination, this->_message.numMessage, this->_message.type); //save the message to provide duplicate
        this->_oldMessageManager.addNewMessage(newSavedMessage);
    }

    if(this->_whatToDo == PREQ_INTER) {
        message.dest=this->_getGoodLongAddress(BROADCAST);
        this->_addYourselfToWayList();
    }

    if(this->_whatToDo == PREP_INTER) {
        message.dest=this->_getGoodLongAddress(BEFOREME);
    }

    if(this->_whatToDo == DATA_INTER) {
        message.dest=this->_getGoodLongAddress(AFTERME);
    }

    if(this->_whatToDo == PREP_DEST) {

        message.dest = this->_getGoodLongAddress(FIRST);
        this->_message.type = TYPE_DATA; // need to change type
        this->_message.numMessage++;

        SavedMessage newSavedMessage(this->_message.source, this->_message.destination, this->_message.numMessage, this->_message.type);
        this->_oldMessageManager.addNewMessage(newSavedMessage);

        //add data
        for(int i = 0; i<this->_sizeDta; i++) {
            this->_message.message[i] = this->_data[i];
            this->_message.messageSize = this->_sizeDta;
        }

        this->_startRebuild(&message);
        return message;
    }

    this->_startRebuild(&message);
    return message;
}

Message DSRprotocol::getMessage(){
    return this->_message;
}
ReadyMessage DSRprotocol::startSendingProcessus(char destination[SMALL_ADDRESS_SIZE], uint8_t data[], int dataSize){

    //first step .... created a request message
    this->sendingStarted = true;
    this->_sizeDta = dataSize;
    for(int i = 0; i<this->_sizeDta; i++)
        this->_data[i] = data[i];

    this->_whatToDo = PREQ_INTER;
    this->_message.source = this->_selfAddress;
    this->_message.destination.setAddress(destination);
    this->_message.messageSize = 0;
    this->_message.routeSize = 0;
    this->_message.type = TYPE_PREQ;

		if (this->_numberSendedMessage == 'Z') {
					this->_numberSendedMessage == '0';
		}
		else
			this->_message.numMessage = this->_numberSendedMessage++;

    ReadyMessage message;

    message.dest = this->_getGoodLongAddress(BROADCAST);

    SavedMessage newSavedMessage(this->_message.source, this->_message.destination, this->_message.numMessage, this->_message.type);
    this->_oldMessageManager.addNewMessage(newSavedMessage);
    this->_startRebuild(&message);
    return message;
}

/*void DSRprotocol::print(){
    this->_oldMessageManager.print();
    this->_addressManager.print();
}*/

/* ----------------------------------------------------------  */
/* ------------------------- TEST ---------------------------  */
/* ----------------------------------------------------------  */
/*
void test(){

    char selfAddress56[] = {'5','6'};
    SmallAddress dsr56smalladdr = SmallAddress(selfAddress56);
    LongAddress dsr56Long = LongAddress(0x00001111,0x00005656);

    char selfAddressd3[] = {'d','3'};
    SmallAddress dsrD3smalladdr = SmallAddress(selfAddressd3);
    LongAddress dsrD3Long = LongAddress(0x00001111,0x000011d3);
    AddressMemElement senderD3;
    senderD3.identifiant = dsrD3smalladdr;
    senderD3.longAddress = dsrD3Long;

    char selfAddress53[] = {'5','3'};
    SmallAddress dsr53smalladdr = SmallAddress(selfAddress53);
    LongAddress dsr53Long = LongAddress(0x00001111,0x00001053);
    AddressMemElement sender53;
    sender53.identifiant = dsr53smalladdr;
    sender53.longAddress = dsr53Long;

    DSRprotocol dsr56 = DSRprotocol(selfAddress56);
    dsr56.networkSimulator.startNetworkSimulation();
    dsr56.networkSimulator.addToWhiteList(dsr53smalladdr);
    dsr56.networkSimulator.addToWhiteList(dsrD3smalladdr);
    // -----------------| t |num |  dest | source| route | ....  | ....  | ....  | ....  | ....  | ....  | ....  | ....  | ....  | data |
    uint8_t message[] =  {'P','2','5','6','e','5','d','3'};
    uint8_t data[] = {'c','o','u','c','o','u'};

    ReadyMessage messageReady;
        cout << "--------------------------- Start ------------------------ 555 " << endl;

    char selfAddress69[] = {'6','9'};
    SmallAddress dsr69smalladdr = SmallAddress(selfAddress69);

    //test envoie debut envoie de message
    //attendu -> Q06956
    //result -> Q06956
    messageReady = dsr56.startSendingProcessus(selfAddress56,data,sizeof(data));
    messageReady.print();

    cout << "--------------------------- PREQ_DEST ------------------------ 555 " << endl;
    // PREQ_DEST OK
    // -----------------   | t |num |  dest | source| route | ....  | ....  | ....  | ....  | ....  | ....  | ....  | ....  | ....  | data |
    //test PREQ_DEST 2 noeuds
    uint8_t messageQD2[] = {'Q','1','5','6','d','3'};
    //test PREQ_DEST 3 noeuds
    uint8_t messageQD3[] = {'Q','2','5','6','e','5','d','3'};
    //test PREQ_DEST 4 noeuds
    uint8_t messageQD4[] = {'Q','3','5','6','e','5','p','5','d','3'};

    //attendu -> P256d356 ok
    dsr56.firstMessageProcessing(messageQD2,sizeof(messageQD2),senderD3);
    messageReady = dsr56.getResponseToSend();
    messageReady.print();

    //attendu -> P256e5d356 ok
    dsr56.firstMessageProcessing(messageQD3,sizeof(messageQD3),senderD3);
    messageReady = dsr56.getResponseToSend();
    messageReady.print();

    //attendu -> P256e5p5d356 ok
    dsr56.firstMessageProcessing(messageQD4,sizeof(messageQD4),senderD3);
    messageReady = dsr56.getResponseToSend();
    messageReady.print();

    cout << "--------------------------- PREQ_INTER ------------------------ 555 " << endl;
    // PREQ_INTER OK
    // -----------------   | t |num |  dest | source| route | ....  | ....  | ....  | ....  | ....  | ....  | ....  | ....  | ....  | data |
    //test PREQ_DEST 2 noeuds
    uint8_t messageQI2[] = {'Q','1','a','a','d','3'};
    //test PREQ_DEST 3 noeuds
    uint8_t messageQI3[] = {'Q','2','a','a','e','5','d','3'};
    //test PREQ_DEST 4 noeuds
    uint8_t messageQI4[] = {'Q','3','a','a','e','5','p','5','d','3'};

    //attendu -> R2aad356 ok
    dsr56.firstMessageProcessing(messageQI2,sizeof(messageQI2),senderD3);
    messageReady = dsr56.getResponseToSend();
    messageReady.print();

    //attendu -> R2aad3e5d356 ok
    dsr56.firstMessageProcessing(messageQI3,sizeof(messageQI3),senderD3);
    messageReady = dsr56.getResponseToSend();
    messageReady.print();

    //attendu -> R2aad3e5p5d356 ok
    dsr56.firstMessageProcessing(messageQI4,sizeof(messageQI4),senderD3);
    messageReady = dsr56.getResponseToSend();
    messageReady.print();


    cout << "--------------------------- PREP_INTER ------------------------ 555 " << endl;
    // PREP_INTER OK
    // -----------------   | t |num |  dest | source| route | ....  | ....  | ....  | ....  | ....  | ....  | ....  | ....  | ....  | data |
    //test PREP_INTER 2 noeuds
    uint8_t messagePI2[] = {'P','1','5','3','d','3','5','6','5','3'};
    //test PREP_INTER 3 noeuds
    uint8_t messagePI3[] = {'P','2','e','5','2','2','5','3','5','6','e','5'};

    //attendu -> P153d35653 ok
    dsr56.firstMessageProcessing(messagePI2,sizeof(messagePI2),sender53);
    messageReady = dsr56.getResponseToSend();
    messageReady.print();

    //attendu ->  P25353d356e5 ok
    dsr56.firstMessageProcessing(messagePI3,sizeof(messagePI3),senderD3);
    messageReady = dsr56.getResponseToSend();
    messageReady.print();

    cout << "------------------------- PREP_DEST ------------------------- 555 " << endl;

    // PREP_DEST OK
    // -----------------   | t |num |  dest | source| route | ....  | ....  | ....  | ....  | ....  | ....  | ....  | ....  | ....  | data |
    //test PREP_DEST 2 noeuds
    uint8_t messagePD2[] = {'P','5','d','3','5','6','d','3'};
    //test PREP_DEST 3 noeuds
    uint8_t messagePD3[] = {'P','6','e','5','5','6','5','3','3','a','e','5','3','a','3','a'};


    //attendu -> P153d35653 ok
    int i = dsr56.firstMessageProcessing(messagePD2,sizeof(messagePD2),senderD3);
    if(i == PREP_DEST){
        messageReady = dsr56.getResponseToSend();
        messageReady.print();
    }

    //attendu ->  P25353d356e5 ok
    i = dsr56.firstMessageProcessing(messagePD3,sizeof(messagePD3),sender53);
    if(i == PREP_DEST){
        messageReady = dsr56.getResponseToSend();
        messageReady.print();
    }

    cout << "--------------------------- DATA_INTER ------------------------ 555 " << endl;

    // DATA_INTER OK
    // -----------------   | t |num |  dest | source| route | ....  | ....  | ....  | ....  | ....  | ....  | ....  | ....  | ....  | data |
    //test DATA_INTER 2 noeuds
    uint8_t messageDI2[] = {'D','5','d','3','5','3','5','6','d','3','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','S','A','L','U','T','\0'};

    //attendu -> P153d35653 ok
    i = dsr56.firstMessageProcessing(messageDI2,sizeof(messageDI2),sender53);
    messageReady = dsr56.getResponseToSend();
    messageReady.print();

    cout << "--------------------------- DATA_DEST ------------------------ 555 " << endl;
    // DATA_DEST OK
    // -----------------   | t |num |  dest | source| route | ....  | ....  | ....  | ....  | ....  | ....  | ....  | ....  | ....  | data |
    //test DATA_DEST 2 noeuds
    uint8_t messageDD2[] = {'D','5','5','6','5','3','5','6','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','S','A','L','U','T','\0'};

    //attendu -> P153d35653 ok
    i = dsr56.firstMessageProcessing(messageDD2,sizeof(messageDD2),sender53);
    messageReady = dsr56.getResponseToSend();
    if(i == DATA_DEST){
        Message message = dsr56.getMessage();
        message.print();
    }
}
*//*
int main(int argc, char** argv) {
    //test();
    return 0;
}*/

/*
 char hexString[4*sizeof(int)+1];
 // returns decimal value of hex
 sprintf(hexString,"%x", LongP2); */
