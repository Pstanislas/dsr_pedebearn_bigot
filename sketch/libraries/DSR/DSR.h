#ifndef DRS_H
#define DRS_H

#if defined(ARDUINO) && ARDUINO >= 100
	#include "Arduino.h"
#endif

// MESSAGE CONSTANTS
#define PATH_MAX_SIZE 10
#define MESSAGE_CONTENT_MAX_SIZE 50
#define MESSAGE_TOTAL_MAX_SIZE 96

#define TYPE_START 0
#define SOURCE_START 4
#define NUM_START 1
#define DEST_START 2
#define ROUTE_START 6
#define MESSAGE_START 26

#define TYPE_PREQ 'Q'
#define TYPE_PREP 'P'
#define TYPE_DATA 'M'
#define DATA_NULL '\0'

// DSR CONSTANTS
#define OLD_MESSAGES_BUFFER_SIZE 10
#define ADDRESS_BUFFER_SIZE 10

// ADDRESS CONSTANTS
#define MAC_ADDRESS_SIZE 8
#define SMALL_ADDRESS_SIZE 2
#define MAX_ADDRESS_MEMORY_SIZE 10
#define BROADCAST1 0x00000000
#define BROADCAST2 0x0000ffff

#define PREQ_DEST 11
#define PREQ_INTER 12
#define PREP_DEST 21
#define PREP_INTER 22
#define DATA_DEST 31
#define DATA_INTER 32
#define DATA_SEND 33
#define NOTHING 0

#define FIRST 0
#define LAST 2
#define BEFOREME -1
#define AFTERME 1
#define BROADCAST -2

#define MAX_SIZE_WHITE_LIST 10

// Definition of a long address : 2 parts of MAC address.
class LongAddress{
public:
    uint32_t P1address;
    uint32_t P2address;

    LongAddress();
    LongAddress(uint32_t P1, uint32_t P2);
    //void print();
};

// Definition of a short address used for TP.
class SmallAddress{
public:
    SmallAddress();
    SmallAddress(char[SMALL_ADDRESS_SIZE]);

    char address[SMALL_ADDRESS_SIZE];
    void setAddress(char[SMALL_ADDRESS_SIZE]);
    //void print();
};

// LongAddress linked to a small one
class AddressMemElement{
public:
    SmallAddress identifiant;
    LongAddress longAddress;
};

// Message structuration.
class Message{

public:

    SmallAddress source;
    SmallAddress destination;
    SmallAddress route[PATH_MAX_SIZE];

    char message[MESSAGE_CONTENT_MAX_SIZE];
    char numMessage;
    char type;
    int messageSize;

    int routeSize;
    int size;

    //void print();

};

// Provide all information you need to seen a message (message + size + addr)
class ReadyMessage{
public:

    LongAddress dest;
    uint8_t message[MESSAGE_TOTAL_MAX_SIZE];
    int size;

    ReadyMessage();
    //void print();
};

// Old message to check duplication.
class SavedMessage{
public:
    SmallAddress sender;
    SmallAddress receiver;
    char numMessage;
    char messageType;

    SavedMessage();
    SavedMessage(SmallAddress, SmallAddress, char, char);
    //void print();
};

//Usefull to make network simulation
class NetworkSimulation{
private:
    bool _etat;
    int _whiteListSize;
    SmallAddress _whiteList[MAX_SIZE_WHITE_LIST];

public:
    NetworkSimulation();

    bool canPass(SmallAddress); //to know if the sender is a neighbour

    void startNetworkSimulation(); // enable networkSimulation
    void stopNetworkSimulation(); // strop networkSimulation
    bool getEtat();

    void addToWhiteList(SmallAddress); // add someone to the white list
    void supprOfWhiteList(SmallAddress); // supp someone to the white list

    //void print();
};

//Manage and provide duplicated messages
class OldMessageManager{
    // Management of old messages to prevent duplicated messages
private:
    SavedMessage _listOldMessage[OLD_MESSAGES_BUFFER_SIZE];
    int _sizeListOldMessage;
    int _iteratorListOldMessage;

public :

    OldMessageManager();
    int getSizeList(); //get number of olded message
    int getIteratorListOldMessage(); //too simulate a circular array
    SavedMessage& getOldSavedMessage();
    bool addNewMessage(SavedMessage); //add and check id it's not a duplicated message
    //void print();
};

//Usefull to memorize mac address and her small one
class AddressManager{
private:

    AddressMemElement _listOfAddress[MAX_ADDRESS_MEMORY_SIZE];
    int _sizeOfSavedAddress;
    bool _isADuplicatedAddress(AddressMemElement);


public:
    AddressManager();
    LongAddress getConrrespondingLongAddress(SmallAddress);
    void addAddress(AddressMemElement);
    void print();

};

// DSR protocol, the master class ...
class DSRprotocol{
private:

    OldMessageManager _oldMessageManager;
    AddressManager _addressManager;

    Message _message;
    SmallAddress _selfAddress;

    uint8_t _data[MESSAGE_CONTENT_MAX_SIZE]; //data how need to be sended
    int _sizeDta;

    int _role;
    int _whatToDo;
    char _numberSendedMessage; //to change the message number

    // Defined the role for this message (0 = NOTHING / 1 = RECIPIENT  / 2 = MEDIATOR / 3 = SENDER)
    int _wayTreatment(uint8_t[]); // read the char array to know whore are differents elements of the way list
    int _set_Role(); //define the role
    void _addYourselfToWayList(); // add your small address a the end of the way
    void _startRebuild(ReadyMessage *); // transform a Message to a char Array

    LongAddress _getGoodLongAddress(int);

public :

    NetworkSimulation networkSimulator;
    bool sendingStarted; //if a transaction is started but finished

    DSRprotocol(char[SMALL_ADDRESS_SIZE]); //Constructor who take the small user address

    //Read the char table message to fill the message class and setUp the role if it's not an old message (echo)
    int firstMessageProcessing(uint8_t[], int, AddressMemElement);

    //To know what to do with this message
    // return PREQ_DEST if you are the destination of a request message
    // return PREQ_INTER if you are the meddiator of a request message
    // return PREP_DEST .....
    // return PREP_INTER ...
    // ...
    // the full list is provided at to of this document
    int getWhatToDo();
    ReadyMessage getResponseToSend(); // return an object composed by : a char array who represent the message, his size, and the mac address of the recipient
    Message getMessage(); // return the instant of the last message received
    ReadyMessage startSendingProcessus(char[SMALL_ADDRESS_SIZE],uint8_t[],int); // to initiate a connection. Return an object composed by : a char array who represent the message, his size, and the mac address of the recipient

    //void print();
};
#endif
