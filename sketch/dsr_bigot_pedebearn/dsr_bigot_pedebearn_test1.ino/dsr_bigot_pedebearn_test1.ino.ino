#include <DSR.h>
#include <XBee.h>
#define XBEE_UART1
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

char destinataire[] = {'e','5'};
char selfAddress[] = {'e','9'};
char data[] = {'c','o','u','c','o','u',' ','d','e',' ','e','9'};

//définition des addresses des voisins
SmallAddress smallneighbor1 = SmallAddress((uint8_t[]){'e','5'});
SmallAddress smallneighbor2 = SmallAddress((uint8_t[]){'3','6'});

AddressMemElement addressInfo;
ReadyMessage messageInfos;

//Instanciation du proocole DSR
DSRprotocol dsr = DSRprotocol(selfAddress);

//Xbee variables
XBee xbee = XBee();
uint8_t* payload;
uint8_t payload_len;
Rx16Response rx16 = Rx16Response();
Rx64Response rx64 = Rx64Response();

LiquidCrystal_I2C lcd(0x20,16,2);

int j=0;

void printLCD(int theLine, const char* theInfo, boolean clearFirst=true) {
  if (clearFirst)
        lcd.clear();
  lcd.setCursor(0, theLine);
  lcd.print(theInfo);
}

void setup() {
  //Activation de la simulation de réseau
  dsr.networkSimulator.addToWhiteList(smallneighbor1);
  dsr.networkSimulator.addToWhiteList(smallneighbor2);
  dsr.networkSimulator.startNetworkSimulation();

  //lcd setup
  lcd.begin();
  lcd.backlight();
  lcd.home();
  lcd.print("Wayting message");
  
  Serial.begin(38400);
  Serial.println("Arduino. Will send packets.");
  #ifdef XBEE_UART1
    xbee.setSerial(Serial1);
    Serial1.begin(38400);
  #else
    xbee.begin(38400);
  #endif
}

void loop() {

  j++;
  
  if(j>1000){
    j=0;
    startEnvoiMessage();
  }
  
  xbee.readPacket();

  //Si message dispo
  if (xbee.getResponse().isAvailable()) {
    // Test si message envoyé précedement est correctement envoyé
    if (xbee.getResponse().getApiId() == TX_STATUS_RESPONSE) {                
      TxStatusResponse txStatus = TxStatusResponse();
      xbee.getResponse().getTxStatusResponse(txStatus);   
      if (txStatus.isSuccess()) {
            Serial.println("Send successful");
      } else
        Serial.println("Send failed");
      
      Serial.println("--------------------------------");
      Serial.println();
      Serial.println();
      Serial.println();             
    }
    else{
      recuperationMessage();
      addressInfo = recuperationAdresses();
      int toDo = dsr.firstMessageProcessing(payload,payload_len,addressInfo); //premier traitement sur le message récupéré
  
      switch(toDo){
          case PREQ_DEST:
          case PREQ_INTER:  
          case PREP_INTER:
          case DATA_INTER:
          case PREP_DEST:
            arduinoLCDspecificAffichage(toDo);
            messageInfos = dsr.getResponseToSend();
            envoiMessage(messageInfos);
            break;
          case DATA_DEST:
            afficherMessage();
            break;
          case NOTHING:
            arduinoLCDspecificAffichage(toDo);
            Serial.println("Message non traité ..."); 
            break;
      }
    }
  }
  delay(10);
}


void startEnvoiMessage(){
  messageInfos = dsr.startSendingProcessus(destinataire,data,sizeof(data));
  envoiMessage(messageInfos); 
}

//Fonction qui permet l'envoi du message
void envoiMessage(ReadyMessage messageInfos){
  
  XBeeAddress64 addr64 = XBeeAddress64(messageInfos.dest.P1address, messageInfos.dest.P2address);
  
  Serial.print("message à envoyé : ");
  for (int i=0; i<messageInfos.size; i++)
      Serial.print((char)messageInfos.message[i]);

  Serial.print(" addresse : ");
  Serial.print(addr64.getMsb(), HEX);
  Serial.print(",");
  Serial.println(addr64.getLsb(), HEX);
  Serial.println("----------"); 

  //envoie
  Tx64Request tx = Tx64Request(addr64, (uint8_t*)messageInfos.message, messageInfos.size);
  xbee.send(tx);
}

//Recupération du message avec XBEE
void recuperationMessage(){
  if (xbee.getResponse().getApiId() == RX_16_RESPONSE || xbee.getResponse().getApiId() == RX_64_RESPONSE) {

    if (xbee.getResponse().getApiId() == RX_16_RESPONSE) {
      xbee.getResponse().getRx16Response(rx16); 
      payload = rx16.getData();
      payload_len=rx64.getDataLength();     
    } 
    else 
    {
      xbee.getResponse().getRx64Response(rx64);
      payload = rx64.getData();
      payload_len=rx64.getDataLength();
    }
    Serial.println("--------------------------------");
    Serial.print("Message recu : ");
    for (int i=0; i<payload_len; i++)
      Serial.print((char)payload[i]);

    //affichage des information de l'expéditeur
    Serial.print(" Provenant de : ");
    Serial.print(addressInfo.identifiant.address[0]);
    Serial.print(addressInfo.identifiant.address[1]);
    Serial.print("   ");
    Serial.print(addressInfo.longAddress.P1address, HEX);
    Serial.print(",");
    Serial.println(addressInfo.longAddress.P2address, HEX);
    Serial.println("----------");
  }
}

//Récupération des information de la source (address mac + équivalent petite addresse)
AddressMemElement recuperationAdresses(){

  XBeeAddress64  sender = rx64.getRemoteAddress64();
  AddressMemElement senderInfos = AddressMemElement();
  LongAddress senderLongAddress = LongAddress(sender.getMsb(),sender.getLsb());

  //Transformation d'un int hexa en chaine de caractères
  char hexString[4*sizeof(char)+1];
  sprintf(hexString,"%x", sender.getLsb());

  senderInfos.longAddress = senderLongAddress;
  senderInfos.identifiant.address[0] = hexString[2];
  senderInfos.identifiant.address[1] = hexString[3];

  return senderInfos;
}

//Affiche un objet de type Message
void afficherMessage(){
  Message message = dsr.getMessage();
  Serial.print("Un message pour toi : ");
  for(int i=0; i<message.messageSize; i++)
    Serial.print(message.message[i]);
  Serial.println();
  Serial.println("--------------------------------");
  Serial.println();
  Serial.println();
  Serial.println(); 
  
  //lcd Part
  printLCD(0,"Last event :", true);
  printLCD(1,message.message, false); 
}

void arduinoLCDspecificAffichage(int toDo){
    printLCD(0,"Last event :", true);
    switch(toDo){
          case PREQ_DEST:
            printLCD(1,"recep PREQ_DEST", false); 
            break;
          case PREQ_INTER:
            printLCD(1,"recep PREQ_INTER", false); 
            break; 
          case PREP_INTER:
            printLCD(1,"recep PREP_INTER", false); 
            break;
          case DATA_INTER:
            printLCD(1,"recep DATA_INTER", false); 
            break;
          case PREP_DEST:
            printLCD(1,"recep PREP_DEST", false); 
            break;
          case NOTHING:
            printLCD(1,"bad Mess", false); 
            break;
      }
}
