L'implémentation du DSR se trouve dans le fichier :
- sketch/libraries/DSR/

L'implémentation du DSR sur Arduino et les tests se trouvent :
- sketch/dsr_bigot_pedebearn/

Le dossier sketch présent dans ce dossier est utilisable comme répertoire sketch sur Arduino (testé avec v1.8)
